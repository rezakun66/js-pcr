// const http = require('http');
// const hostname = '127.0.0.1';
// const port= 30000;

// const server= http.createServer((req,res)=>{
//     res.statusCode=200;
//     res.setHeader('Contents-type','text/plan');
//     res.end("hella word");

// } );


// server.listen(port, hostname, () => {
//     console.log(`App Running on port ${port}`);
// });

const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const mongoose = require ('mongoose');
const app = express();

const mongoUrl='mongodb+srv://admin:admin@cluster0-yptpw.gcp.mongodb.net/test?retryWrites=true&w=majority';
const connetDB = () =>
    mongoose.connect(mongoUrl,{
        useNewUrlParser:true,
        useCreateIndex:true,
    })
        .then(()=> console.log('DB Connected'))
        .catch(()=>console.log('failed to Connect DB'));

connetDB();

app.use(bodyParser.json());
app.use(cors());
app.use(helmet());

const {
    userList,
    addUser,
    getUserById,
    editUser,
    deleteUser,
} = require('./modules/users');

const {
    login,
} = require('./modules/auth');

app.get('/users', userList);

app.get('/user/:id', getUserById);

app.put('/edit/:id', editUser);

app.delete('/delete/:id', deleteUser);

app.post('/login', login);

app.post('/users', addUser);

app.listen(4000, () => {
    console.log(`App Running Up!`)
});
